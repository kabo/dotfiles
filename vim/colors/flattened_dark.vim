" 'flattened_dark.vim' -- Vim color scheme.
" Maintainer:   Romain Lafourcade (romainlafourcade@gmail.com)
" Description:  Dark Solarized, without the bullshit.
" https://github.com/dexpota/kitty-themes/blob/master/themes/Solarized_Dark_-_Patched.conf
" https://github.com/romainl/flattened/blob/master/colors/flattened_dark.vim

hi clear

if exists('syntax_on')
  syntax reset
endif

let colors_name = 'flattened_dark'

hi  Normal                                    guifg=#839496  guibg=#001e26  gui=NONE

set background=dark

hi  ColorColumn                               guibg=#002731  gui=NONE
hi  Comment                                   guifg=#586e75  gui=italic
hi  ConId                                     guifg=#b58900  gui=NONE
hi  Conceal                                   guifg=#268bd2  guibg=#001e26  gui=NONE
hi  Constant                                  guifg=#2aa198  gui=NONE
hi  Cursor                                    guifg=#001e26  guibg=#839496  gui=NONE
hi  CursorColumn                              guibg=#002731  gui=NONE
hi  CursorLine                                guibg=#002731  guisp=#93a1a1  gui=NONE
hi  CursorLineNr                              guifg=#b58900  gui=NONE
hi  DiffAdd                                   guifg=#719e07  guibg=#002731  guisp=#719e07  gui=NONE
hi  DiffChange                                guifg=#b58900  guibg=#002731  guisp=#b58900  gui=NONE
hi  DiffDelete                                guifg=#dc322f  guibg=#002731  gui=NONE
hi  DiffText                                  guifg=#268bd2  guibg=#002731  guisp=#268bd2  gui=NONE
hi  Directory                                 guifg=#268bd2  gui=NONE
hi  Error                                     guifg=#dc322f  guibg=#001e26  gui=NONE
hi  ErrorMsg                                  guifg=#dc322f  guibg=NONE gui=reverse
hi  FoldColumn                                guifg=#839496  guibg=#002731  gui=NONE
hi  Folded                                    guifg=#839496  guibg=#002731  guisp=#001e26  gui=NONE
hi  HelpExample                               guifg=#93a1a1  gui=NONE
hi  Identifier                                guifg=#268bd2  gui=NONE
hi  IncSearch                                 guifg=#cb4b16  gui=standout
hi  LineNr                                    guifg=#586e75  guibg=#002731  gui=NONE
hi  MatchParen                                guifg=#dc322f  guibg=#586e75  gui=NONE
hi  ModeMsg                                   guifg=#268bd2  gui=NONE
hi  MoreMsg                                   guifg=#268bd2  gui=NONE
hi  NonText                                   guifg=#657b83  gui=NONE
hi  Pmenu                                     guifg=#839496  guibg=#002731  gui=reverse
hi  PmenuSbar                                 guifg=#eee8d5  guibg=#839496  gui=reverse
hi  PmenuSel                                  guifg=#586e75  guibg=#eee8d5  gui=reverse
hi  PmenuThumb                                guifg=#839496  guibg=#001e26  gui=reverse
hi  PreProc                                   guifg=#cb4b16  gui=NONE
hi  Question                                  guifg=#2aa198  gui=NONE
hi  Search                                    guifg=#b58900  guibg=NONE  gui=reverse
hi  SignColumn                                guifg=#839496  guibg=NONE  gui=NONE
hi  Special                                   guifg=#dc322f  gui=NONE
hi  SpecialKey                                guifg=#657b83  guibg=#002731  gui=NONE
hi  SpellBad                                  guisp=#dc322f  gui=undercurl
hi  SpellCap                                  guisp=#6c71c4  gui=undercurl
hi  SpellLocal                                guisp=#b58900  gui=undercurl
hi  SpellRare                                 guisp=#2aa198  gui=undercurl
hi  Statement                                 guifg=#719e07  gui=NONE
hi  StatusLine                                guifg=#93a1a1  guibg=#002731  gui=reverse
hi  StatusLineNC                              guifg=#657b83  guibg=#002731  gui=reverse
hi  TabLine                                   guifg=#839496  guibg=#002731  guisp=#839496  gui=underline
hi  TabLineFill                               guifg=#839496  guibg=#002731  guisp=#839496  gui=underline
hi  TabLineSel                                guifg=#586e75  guibg=#eee8d5  guisp=#839496  gui=underline,reverse
hi  Title                                     guifg=#cb4b16  gui=NONE
hi  Todo                                      guifg=#d33682  guibg=NONE  gui=bold
hi  Type                                      guifg=#b58900  gui=NONE
hi  Underlined                                guifg=#6c71c4  gui=NONE
hi  VarId                                     guifg=#268bd2  gui=NONE
hi  VertSplit                                 guifg=#657b83  guibg=#657b83  gui=NONE
hi  Visual                                    guifg=#586e75  guibg=#001e26  gui=reverse
hi  VisualNOS                                 guibg=#002731  guifg=NONE  gui=reverse
hi  WarningMsg                                guifg=#dc322f  gui=NONE
hi  WildMenu                                  guifg=#eee8d5  guibg=#002731 gui=reverse

hi  cPreCondit                                guifg=#cb4b16  gui=NONE

hi  gitcommitBranch                           guifg=#d33682  gui=NONE
hi  gitcommitComment                          guifg=#586e75  gui=italic
hi  gitcommitDiscardedFile                    guifg=#dc322f  gui=NONE
hi  gitcommitDiscardedType                    guifg=#dc322f  gui=NONE
hi  gitcommitFile                             guifg=#839496  gui=NONE
hi  gitcommitHeader                           guifg=#586e75  gui=NONE
hi  gitcommitOnBranch                         guifg=#586e75  gui=NONE
hi  gitcommitSelectedFile                     guifg=#719e07  gui=NONE
hi  gitcommitSelectedType                     guifg=#719e07  gui=NONE
hi  gitcommitUnmerged                         guifg=#719e07  gui=NONE
hi  gitcommitUnmergedFile                     guifg=#b58900  gui=NONE
hi  gitcommitUntrackedFile                    guifg=#2aa198  gui=NONE

hi  helpHyperTextEntry                        guifg=#719e07  gui=NONE
hi  helpHyperTextJump                         guifg=#268bd2  gui=underline
hi  helpNote                                  guifg=#d33682  gui=NONE
hi  helpOption                                guifg=#2aa198  gui=NONE
hi  helpVim                                   guifg=#d33682  gui=NONE

hi  hsImport                                  guifg=#d33682  gui=NONE
hi  hsImportLabel                             guifg=#2aa198  gui=NONE
hi  hsModuleName                              guifg=#719e07  gui=underline
hi  hsNiceOperator                            guifg=#2aa198  gui=NONE
hi  hsStatement                               guifg=#2aa198  gui=NONE
hi  hsString                                  guifg=#657b83  gui=NONE
hi  hsStructure                               guifg=#2aa198  gui=NONE
hi  hsType                                    guifg=#b58900  gui=NONE
hi  hsTypedef                                 guifg=#2aa198  gui=NONE
hi  hsVarSym                                  guifg=#2aa198  gui=NONE
hi  hs_DeclareFunction                        guifg=#cb4b16  gui=NONE
hi  hs_OpFunctionName                         guifg=#b58900  gui=NONE
hi  hs_hlFunctionName                         guifg=#268bd2  gui=NONE

hi  htmlArg                                   guifg=#657b83  gui=NONE
hi  htmlEndTag                                guifg=#586e75  gui=NONE
hi  htmlSpecialTagName                        guifg=#268bd2  gui=italic
hi  htmlTag                                   guifg=#586e75  gui=NONE
hi  htmlTagN                                  guifg=#93a1a1  gui=NONE
hi  htmlTagName                               guifg=#268bd2  gui=NONE

hi  javaScript                                guifg=#b58900  gui=NONE

hi  pandocBlockQuote                          guifg=#268bd2  gui=NONE
hi  pandocBlockQuoteLeader1                   guifg=#268bd2  gui=NONE
hi  pandocBlockQuoteLeader2                   guifg=#2aa198  gui=NONE
hi  pandocBlockQuoteLeader3                   guifg=#b58900  gui=NONE
hi  pandocBlockQuoteLeader4                   guifg=#dc322f  gui=NONE
hi  pandocBlockQuoteLeader5                   guifg=#839496  gui=NONE
hi  pandocBlockQuoteLeader6                   guifg=#586e75  gui=NONE
hi  pandocCitation                            guifg=#d33682  gui=NONE
hi  pandocCitationDelim                       guifg=#d33682  gui=NONE
hi  pandocCitationID                          guifg=#d33682  gui=underline
hi  pandocCitationRef                         guifg=#d33682  gui=NONE
hi  pandocComment                             guifg=#586e75  gui=italic
hi  pandocDefinitionBlock                     guifg=#6c71c4  gui=NONE
hi  pandocDefinitionIndctr                    guifg=#6c71c4  gui=NONE
hi  pandocDefinitionTerm                      guifg=#6c71c4  gui=standout
hi  pandocEmphasis                            guifg=#839496  gui=italic
hi  pandocEmphasisDefinition                  guifg=#6c71c4  gui=italic
hi  pandocEmphasisHeading                     guifg=#cb4b16  gui=NONE
hi  pandocEmphasisNested                      guifg=#839496  gui=NONE
hi  pandocEmphasisNestedDefinition            guifg=#6c71c4  gui=NONE
hi  pandocEmphasisNestedHeading               guifg=#cb4b16  gui=NONE
hi  pandocEmphasisNestedTable                 guifg=#268bd2  gui=NONE
hi  pandocEmphasisTable                       guifg=#268bd2  gui=italic
hi  pandocEscapePair                          guifg=#dc322f  gui=NONE
hi  pandocFootnote                            guifg=#719e07  gui=NONE
hi  pandocFootnoteDefLink                     guifg=#719e07  gui=NONE
hi  pandocFootnoteInline                      guifg=#719e07  gui=NONE,underline
hi  pandocFootnoteLink                        guifg=#719e07  gui=underline
hi  pandocHeading                             guifg=#cb4b16  gui=NONE
hi  pandocHeadingMarker                       guifg=#b58900  gui=NONE
hi  pandocImageCaption                        guifg=#6c71c4  gui=NONE,underline
hi  pandocLinkDefinition                      guifg=#2aa198  guisp=#657b83  gui=underline
hi  pandocLinkDefinitionID                    guifg=#268bd2  gui=NONE
hi  pandocLinkDelim                           guifg=#586e75  gui=NONE
hi  pandocLinkLabel                           guifg=#268bd2  gui=underline
hi  pandocLinkText                            guifg=#268bd2  gui=NONE,underline
hi  pandocLinkTitle                           guifg=#657b83  gui=underline
hi  pandocLinkTitleDelim                      guifg=#586e75  guisp=#657b83  gui=underline
hi  pandocLinkURL                             guifg=#657b83  gui=underline
hi  pandocListMarker                          guifg=#d33682  gui=NONE
hi  pandocListReference                       guifg=#d33682  gui=underline
hi  pandocMetadata                            guifg=#268bd2  gui=NONE
hi  pandocMetadataDelim                       guifg=#586e75  gui=NONE
hi  pandocMetadataKey                         guifg=#268bd2  gui=NONE
hi  pandocNonBreakingSpace                    guifg=#dc322f  guibg=NONE  gui=reverse
hi  pandocRule                                guifg=#268bd2  gui=NONE
hi  pandocRuleLine                            guifg=#268bd2  gui=NONE
hi  pandocStrikeout                           guibg=NONE  guifg=#586e75  gui=reverse
hi  pandocStrikeoutDefinition                 guibg=NONE  guifg=#6c71c4  gui=reverse
hi  pandocStrikeoutHeading                    guibg=NONE  guifg=#cb4b16  gui=reverse
hi  pandocStrikeoutTable                      guibg=NONE  guifg=#268bd2  gui=reverse
hi  pandocStrongEmphasis                      guifg=#839496  gui=NONE
hi  pandocStrongEmphasisDefinition            guifg=#6c71c4  gui=NONE
hi  pandocStrongEmphasisEmphasis              guifg=#839496  gui=NONE
hi  pandocStrongEmphasisEmphasisDefinition    guifg=#6c71c4  gui=NONE
hi  pandocStrongEmphasisEmphasisHeading       guifg=#cb4b16  gui=NONE
hi  pandocStrongEmphasisEmphasisTable         guifg=#268bd2  gui=NONE
hi  pandocStrongEmphasisHeading               guifg=#cb4b16  gui=NONE
hi  pandocStrongEmphasisNested                guifg=#839496  gui=NONE
hi  pandocStrongEmphasisNestedDefinition      guifg=#6c71c4  gui=NONE
hi  pandocStrongEmphasisNestedHeading         guifg=#cb4b16  gui=NONE
hi  pandocStrongEmphasisNestedTable           guifg=#268bd2  gui=NONE
hi  pandocStrongEmphasisTable                 guifg=#268bd2  gui=NONE
hi  pandocStyleDelim                          guifg=#586e75  gui=NONE
hi  pandocSubscript                           guifg=#6c71c4  gui=NONE
hi  pandocSubscriptDefinition                 guifg=#6c71c4  gui=NONE
hi  pandocSubscriptHeading                    guifg=#cb4b16  gui=NONE
hi  pandocSubscriptTable                      guifg=#268bd2  gui=NONE
hi  pandocSuperscript                         guifg=#6c71c4  gui=NONE
hi  pandocSuperscriptDefinition               guifg=#6c71c4  gui=NONE
hi  pandocSuperscriptHeading                  guifg=#cb4b16  gui=NONE
hi  pandocSuperscriptTable                    guifg=#268bd2  gui=NONE
hi  pandocTable                               guifg=#268bd2  gui=NONE
hi  pandocTableStructure                      guifg=#268bd2  gui=NONE
hi  pandocTableZebraDark                      guifg=#268bd2  guibg=#002731  gui=NONE
hi  pandocTableZebraLight                     guifg=#268bd2  guibg=#001e26  gui=NONE
hi  pandocTitleBlock                          guifg=#268bd2  gui=NONE
hi  pandocTitleBlockTitle                     guifg=#268bd2  gui=NONE
hi  pandocTitleComment                        guifg=#268bd2  gui=NONE
hi  pandocVerbatimBlock                       guifg=#b58900  gui=NONE
hi  pandocVerbatimInline                      guifg=#b58900  gui=NONE
hi  pandocVerbatimInlineDefinition            guifg=#6c71c4  gui=NONE
hi  pandocVerbatimInlineHeading               guifg=#cb4b16  gui=NONE
hi  pandocVerbatimInlineTable                 guifg=#268bd2  gui=NONE

hi  perlHereDoc                               guifg=#93a1a1  guibg=#001e26  gui=NONE
hi  perlStatementFileDesc                     guifg=#2aa198  guibg=#001e26  gui=NONE
hi  perlVarPlain                              guifg=#b58900  guibg=#001e26  gui=NONE

hi  rubyDefine                                guifg=#93a1a1  guibg=#001e26  gui=NONE

hi  texMathMatcher                            guifg=#b58900  guibg=#001e26  gui=NONE
hi  texMathZoneX                              guifg=#b58900  guibg=#001e26  gui=NONE
hi  texRefLabel                               guifg=#b58900  guibg=#001e26  gui=NONE
hi  texStatement                              guifg=#2aa198  guibg=#001e26  gui=NONE

hi  vimCmdSep                                 guifg=#268bd2  gui=NONE
hi  vimCommand                                guifg=#b58900  gui=NONE
hi  vimCommentString                          guifg=#6c71c4  gui=NONE
hi  vimGroup                                  guifg=#268bd2  gui=NONE,underline
hi  vimHiGroup                                guifg=#268bd2  gui=NONE
hi  vimHiLink                                 guifg=#268bd2  gui=NONE
hi  vimIsCommand                              guifg=#657b83  gui=NONE
hi  vimSynMtchOpt                             guifg=#b58900  gui=NONE
hi  vimSynType                                guifg=#2aa198  gui=NONE

hi link Boolean                             Constant
hi link Character                           Constant
hi link Conditional                         Statement
hi link Debug                               Special
hi link Define                              PreProc
hi link Delimiter                           Special
hi link Exception                           Statement
hi link Float                               Number
hi link Function                            Identifier
hi link HelpCommand                         Statement
hi link Include                             PreProc
hi link Keyword                             Statement
hi link Label                               Statement
hi link Macro                               PreProc
hi link Number                              Constant
hi link Operator                            Statement
hi link PreCondit                           PreProc
hi link Repeat                              Statement
hi link SpecialChar                         Special
hi link SpecialComment                      Special
hi link StorageClass                        Type
hi link String                              Constant
hi link Structure                           Type
hi link SyntasticError                      SpellBad
hi link SyntasticErrorSign                  Error
hi link SyntasticStyleErrorLine             SyntasticErrorLine
hi link SyntasticStyleErrorSign             SyntasticErrorSign
hi link SyntasticStyleWarningLine           SyntasticWarningLine
hi link SyntasticStyleWarningSign           SyntasticWarningSign
hi link SyntasticWarning                    SpellCap
hi link SyntasticWarningSign                Todo
hi link Tag                                 Special
hi link Typedef                             Type

hi link diffAdded                           Statement
hi link diffBDiffer                         WarningMsg
hi link diffCommon                          WarningMsg
hi link diffDiffer                          WarningMsg
hi link diffIdentical                       WarningMsg
hi link diffIsA                             WarningMsg
hi link diffLine                            Identifier
hi link diffNoEOL                           WarningMsg
hi link diffOnly                            WarningMsg
hi link diffRemoved                         WarningMsg

hi link gitcommitDiscarded                  gitcommitComment
hi link gitcommitDiscardedArrow             gitcommitDiscardedFile
hi link gitcommitNoBranch                   gitcommitBranch
hi link gitcommitSelected                   gitcommitComment
hi link gitcommitSelectedArrow              gitcommitSelectedFile
hi link gitcommitUnmergedArrow              gitcommitUnmergedFile
hi link gitcommitUntracked                  gitcommitComment

hi link helpSpecial                         Special

hi link hsDelimTypeExport                   Delimiter
hi link hsImportParams                      Delimiter
hi link hsModuleStartLabel                  hsStructure
hi link hsModuleWhereLabel                  hsModuleStartLabel
hi link htmlLink                            Function

hi link lCursor                             Cursor

hi link pandocCodeBlock                     pandocVerbatimBlock
hi link pandocCodeBlockDelim                pandocVerbatimBlock
hi link pandocEscapedCharacter              pandocEscapePair
hi link pandocLineBreak                     pandocEscapePair
hi link pandocMetadataTitle                 pandocMetadata
hi link pandocTableStructureEnd             pandocTableStructre
hi link pandocTableStructureTop             pandocTableStructre
hi link pandocVerbatimBlockDeep             pandocVerbatimBlock

hi link vimFunc                             Function
hi link vimSet                              Normal
hi link vimSetEqual                         Normal
hi link vimUserFunc                         Function
hi link vipmVar                             Identifier

hi clear SyntasticErrorLine
hi clear SyntasticWarningLine
hi clear helpLeadBlank
hi clear helpNormal
hi clear pandocTableStructre
