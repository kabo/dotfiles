#!/bin/sh

files="vimrc vim gvimrc Xresources Xresources.d tmux.conf tmux_lt_21.conf tmux_ge_21.conf ptconfig.toml eslintrc.yml"
for file in $files; do
				echo "Symlinking ${file}..."
				ln -sf ~/dotfiles/${file} ~/.${file}
done
echo "Done!"

