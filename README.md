# README

## To check out with submodules
```sh
cd ~
git clone https://gitlab.com/kabo/dotfiles.git
cd dotfiles
git submodule update --init --recursive
```

## To update
```sh
cd ~/dotfiles/
git pull
git submodule init
git submodule update --remote
```

## To add submodules
```sh
cd ~/dotfiles/
git submodule add [git-url] [destination]
git commit -m "added submodule"
git push
```

When starting neovim the first time [dein](https://github.com/Shougo/dein.vim) should automatically install the vim plugins.

