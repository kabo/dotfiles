/usr/share/X11/xkb/symbols/us.xkb
```
partial alphanumeric_keys
xkb_symbols "kabo" {

    include "us(basic)"
    name[Group1]= "English (US, kabo)";

    key <AD11> {	[ NoSymbol,	NoSymbol,       aring,          Aring		        ]	};
    key <AC10> {	[ NoSymbol,	NoSymbol,       odiaeresis,     Odiaeresis		]	};
    key <AC11> {	[ NoSymbol,	NoSymbol,       adiaeresis,     Adiaeresis		]	};

    include "eurosign(e)"

    include "level3(ralt_switch)"
};
```

/usr/share/X11/xkb/rules/evdev.xml
```
  <layoutList>
    <layout>
      <configItem>
        <name>us</name>
        ...
      </configItem>
      <variantList>
        <variant>
          <configItem>
            <name>kabo</name>
            <description>English (US, kabo)</description>
          </configItem>
        </variant>
```
