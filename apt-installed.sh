#/bin/sh

# crontab -e
# 07 3    * * * /home/kabo/dotfiles/apt-installed.sh

apt list --installed \
  | tail -n +2 \
  | tee /home/kabo/Documents/apt-installed.txt \
  | grep -v ",automatic]" \
  | tee /home/kabo/Documents/apt-intentionally-installed.txt \
  | cut -d "/" -f 1 > /home/kabo/Documents/apt-intentionally-installed-packages.txt
