scriptencoding utf-8
set encoding=utf-8

" Inspiration:
" https://github.com/ctaylo21/jarvis/blob/master/config/nvim/init.vim
" https://www.freecodecamp.org/news/a-guide-to-modern-web-development-with-neo-vim-333f7efbf8e2/

"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

let g:dein#auto_recache = 1
" Required:
set runtimepath+=/home/kabo/.vim/bundle/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('/home/kabo/.vim/bundle')
  call dein#begin('/home/kabo/.vim/bundle')

  " Let dein manage dein
  " Required:
  call dein#add('/home/kabo/.vim/bundle/repos/github.com/Shougo/dein.vim')

  " Add or remove your plugins here:
  "call dein#add('Shougo/neosnippet.vim')
  "call dein#add('Shougo/neosnippet-snippets')

  " You can specify revision/branch/tag.
  "call dein#add('Shougo/deol.nvim', { 'rev': 'a1b5108fd' })

  call dein#add('vim-airline/vim-airline')          " nice bottom status bar
  " call dein#add('altercation/vim-colors-solarized') " solarized colors in vim
  " call dein#add('ctrlpvim/ctrlp.vim')               " ctrl-p to quickly open files
  call dein#add('christoomey/vim-tmux-navigator')   " to seamlessly navigate splits the same as in tmux
  call dein#add('tpope/vim-unimpaired')             " a few neat commands like [b, [L, and [Space
  call dein#add('tpope/vim-abolish')                " includes case matching substitutions
  " call dein#add('scrooloose/nerdtree')              " file navigator sidebar
  " call dein#add('Shougo/deoplete.nvim')             " autocomplete
  " call dein#add('carlitux/deoplete-ternjs', { 'build': { 'mac': 'npm install -g tern', 'unix': 'npm install -g tern' }}) " autocomplete for javascript
  call dein#add('HerringtonDarkholme/yats.vim')     " syntax highlighting for typescript
  " call dein#add('mhartington/nvim-typescript')      " autocomplete for typescript
  " call dein#add('vim-syntastic/syntastic')          " syntax checking
  " call dein#add('ruanyl/vim-fixmyjs')               " autofix javacript errors
  call dein#add('othree/yajs.vim')                  " javascript syntax highlighting
  call dein#add('mxw/vim-jsx')                      " ReactJS JSX syntax highlighting
  call dein#add('shime/vim-livedown')               " markdown preview
  call dein#add('mzlogin/vim-markdown-toc')         " markdown table of contents
  call dein#add('jparise/vim-graphql')              " graphql syntax
  call dein#add('wannesm/wmgraphviz.vim')           " preview graphviz dot files
  " call dein#add('autozimu/LanguageClient-neovim', { 'rev': 'next', 'build': 'bash install.sh' }) " needed for ReasonML
  " call dein#add('reasonml-editor/vim-reason-plus')  " ReasonML
  call dein#add('junegunn/vim-easy-align')          " Align stuff
  call dein#add('hashivim/vim-terraform')           " Terraform
  call dein#add('tpope/vim-surround')               " work with surrounding stuff with quotes, parens, etc.
  call dein#add('easymotion/vim-easymotion')        " smart motions
  call dein#add('ntpeters/vim-better-whitespace')   " highlight/fix trailing whitespace
  call dein#add('neoclide/coc.nvim', { 'rev': 'release' }) " autocomplete
  call dein#add('Shougo/denite.nvim')               " fuzzy find files and buffers
  " call dein#add('Shougo/echodoc.vim')               " print function signatures in echo area
  call dein#add('mhinz/vim-signify')                " show git changes in sign column
  " call dein#add('purescript-contrib/purescript-vim') " purescript syntax highlighting
  call dein#add('tpope/vim-fireplace')              " show git changes in sign column

  " :CocInstall coc-eslint
  " :CocInstall coc-rls " rust
  " :CocInstall coc-yaml
  " :CocInstall coc-tsserver
  " :CocInstall coc-python
  " :CocInstall coc-json
  " :CocInstall coc-css
  " :CocInstall coc-sh

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif

"End dein Scripts-------------------------

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Make sure we're in nocompat mode
"set nocompatible

" Sets how many lines of history VIM has to remember
set history=700

" Disable mouse
set mouse=""

" Set to auto read when a file is changed from the outside
set autoread

" allow hidden buffers
set hidden

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","
let g:mapleader = ","

" Fast saving
nmap <leader>w :w!<cr>

" Use system clipboard
set clipboard^=unnamedplus
if has("mac") || has("macunix")
  set clipboard=unnamed
endif

" more natural split opening
set splitbelow
set splitright

" enable tab to escape (keepin' it old school)
nnoremap <Tab> <Esc>
vnoremap <Tab> <Esc>gV
onoremap <Tab> <Esc>
" cnoremap <Tab> <C-C><Esc>
inoremap <Tab> <Esc>`^
" inoremap <Leader><Tab> <Tab>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Enable 256 colors in terminal
" set t_Co=256
set termguicolors
colorscheme flattened_dark
"set background=light
" set background=dark
" let g:solarized_termtrans=1
"let g:solarized_visibility = "high"
"let g:solarized_contrast = "high"
" let g:solarized_termcolors=256
" colorscheme solarized
" hi Normal ctermfg=12 ctermbg=0

let g:airline_powerline_fonts = 1
set laststatus=2

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Press <leader>ln to toggle linenos
nmap <leader>ln :set invnumber<CR>
set number

" Highlight current line
set cursorline
" hi CursorLine cterm=NONE ctermbg=233

" Always show current position
set ruler

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expression turn magic on
set magic

" Show macthing brackets when text indicator is over them
set showmatch

" Don't dispay mode in command line (airilne already shows it). Needed for echodoc
set noshowmode

" Don't show last command
set noshowcmd

" Don't give completion messages like 'match 1 of 2' or 'The only match'
set shortmess+=c

" To view hidden chars: :set list, to disable: :set nolist
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<
" this is only for 7.4.710 and above: ,space:␣

" Delete current visual selection and dump in black hole buffer before pasting
" Used when you want to paste over something without it getting copied to
" Vim's default buffer
vnoremap <leader>p "_dP
vnoremap <leader>d "_d
nnoremap <leader>d "_d
vnoremap <leader>c "_c

" Allows you to save files you opened without write permissions via sudo
cmap w!! w !sudo tee %

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Turn backup off, since most stuff is in git anyway...
set nobackup
set nowb
set noswapfile
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,node_modules,.git
set undodir=~/.vim/undodir
set undofile

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=2
set tabstop=2

" Linebreak on 500 characters
set lbr
set tw=500

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Editing mappings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Move a line of text using ALT+[jk] or Command+[jk] on mac
nnoremap <M-j> :m .+1<CR>==
nnoremap <M-k> :m .-2<CR>==
inoremap <M-j> <Esc>:m .+1<CR>==gi
inoremap <M-k> <Esc>:m .-2<CR>==gi
vnoremap <M-j> :m '>+1<CR>gv=gv
vnoremap <M-k> :m '<-2<CR>gv=gv

" Tidy JSON
map <Leader>jt <Esc>:%!python -m json.tool<CR>

" Tidy XML
map <Leader>xt <Esc>:%!tidy -q -i --show-errors 0 -xml<CR>
":command Txml :%!tidy -q -i --show-errors 0 -xml

" Search for visually selected text
vnoremap // y/\V<C-R>=escape(@",'/\')<CR><CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => CTRL-P
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" let g:ctrlp_map = '<c-p>'
" let g:ctrlp_cmd = 'CtrlP'
" let g:ctrlp_custom_ignore = '\v[\/](\.git|\.hg|\.svn|dist|build|coverage)$'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => NERDTree
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" let g:NERDTreeWinPos = "right"
" let NERDTreeIgnore = ['\.pyc$']
" let g:NERDTreeWinSize = 35
" map <C-n> :NERDTreeToggle<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => javascript stuff
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" tsx = typescript
autocmd BufNewFile,BufRead *.tsx setlocal filetype=typescript.tsx
autocmd BufNewFile,BufRead *.jsx setlocal filetype=javascript.jsx
autocmd BufNewFile,BufRead *.purs setlocal filetype=purescript

" Use deoplete.
" let g:deoplete#enable_at_startup = 1

" syntastic
" let g:syntastic_javascript_checkers = ['eslint'] " ['eslint', 'flow']
" let g:syntastic_javascript_eslint_exec = 'eslint_d'
" let g:syntastic_javascript_eslint_args = ['--fix']
" let g:syntastic_typescript_checkers = ['eslint']
" let g:syntastic_typescript_checkers = ['tslint']
" let g:syntastic_typescript_eslint_exec = 'eslint_d'
" let g:syntastic_typescript_tslint_args = ['--fix']
" let g:syntastic_javascript_flow_exe = 'flow'
" let g:syntastic_check_on_open = 1
" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_aggregate_errors = 1

" fixmyjs
" let g:fixmyjs_engine = 'eslint'
" let g:fixmyjs_rc_path = '.eslintrc.yml'

" autoreload files after --fix has fixed stuff ?
" function! SyntasticCheckHook(errors)
  " checktime
" endfunction
" au VimEnter *.ts au BufWritePost *.ts checktime

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => YAML
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

autocmd BufNewFile,BufRead *.{yaml,yml} setlocal foldmethod=indent

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Markdown preview
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:livedown_browser = "brave-browser"

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => GraphViz preview
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:WMGraphviz_output = "png"
map <Leader>vc <Esc>:GraphvizCompile<CR>
map <Leader>vv <Esc>:GraphvizShow<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => ReasonML
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" let g:LanguageClient_serverCommands = {
"     \ 'reason': ['ocaml-language-server', '--stdio'],
"     \ 'ocaml': ['ocaml-language-server', '--stdio'],
"     \ }
"
" nnoremap <F5> :call LanguageClient_contextMenu()<CR>
" Or map each action separately
" nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
" nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
" nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>
" nnoremap <silent> gd :call LanguageClient_textDocument_definition()<cr>
" nnoremap <silent> gf :call LanguageClient_textDocument_formatting()<cr>
" nnoremap <silent> <cr> :call LanguageClient_textDocument_hover()<cr>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => vim-easy-align
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => denite
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Wrap in try/catch to avoid errors on initial install before plugin is available
try
" === Denite setup ==="
" Use ripgrep for searching current directory for files
" By default, ripgrep will respect rules in .gitignore
"   --files: Print each file that would be searched (but don't search)
"   --glob:  Include or exclues files for searching that match the given glob
"            (aka ignore .git files)
"
call denite#custom#var('file/rec', 'command', ['rg', '--files', '--glob', '!.git'])

" Use ripgrep in place of "grep"
call denite#custom#var('grep', 'command', ['rg'])

" Custom options for ripgrep
"   --vimgrep:  Show results with every match on it's own line
"   --hidden:   Search hidden directories and files
"   --heading:  Show the file name above clusters of matches from each file
"   --S:        Search case insensitively if the pattern is all lowercase
call denite#custom#var('grep', 'default_opts', ['--hidden', '--vimgrep', '--heading', '-S'])

" Recommended defaults for ripgrep via Denite docs
call denite#custom#var('grep', 'recursive_opts', [])
call denite#custom#var('grep', 'pattern_opt', ['--regexp'])
call denite#custom#var('grep', 'separator', ['--'])
call denite#custom#var('grep', 'final_opts', [])

" Remove date from buffer list
call denite#custom#var('buffer', 'date_format', '')

" Custom options for Denite
"   auto_resize             - Auto resize the Denite window height automatically.
"   prompt                  - Customize denite prompt
"   direction               - Specify Denite window direction as directly below current pane
"   winminheight            - Specify min height for Denite window
"   highlight_mode_insert   - Specify h1-CursorLine in insert mode
"   prompt_highlight        - Specify color of prompt
"   highlight_matched_char  - Matched characters highlight
"   highlight_matched_range - matched range highlight
let s:denite_options = {'default' : {
\ 'split': 'floating',
\ 'start_filter': 1,
\ 'auto_resize': 1,
\ 'source_names': 'short',
\ 'prompt': 'λ:',
\ 'statusline': 0,
\ 'highlight_matched_char': 'WildMenu',
\ 'highlight_matched_range': 'Visual',
\ 'highlight_window_background': 'Visual',
\ 'highlight_filter_background': 'StatusLine',
\ 'highlight_prompt': 'StatusLine',
\ 'winrow': 1,
\ 'vertical_preview': 1
\ }}

" Loop through denite options and enable them
function! s:profile(opts) abort
  for l:fname in keys(a:opts)
    for l:dopt in keys(a:opts[l:fname])
      call denite#custom#option(l:fname, l:dopt, a:opts[l:fname][l:dopt])
    endfor
  endfor
endfunction

call s:profile(s:denite_options)
catch
  echo 'Denite not installed. It should work after running :PlugInstall'
endtry

" === Denite shorcuts === "
"   ;         - Browser currently open buffers
"   <leader>t - Browse list of files in current directory
"   <leader>g - Search current directory for occurences of given term and close window if no results
"   <leader>j - Search current directory for occurences of word under cursor
" nmap ; :Denite buffer<CR>
nmap <leader>t :Denite buffer file/rec<CR>
nnoremap <leader>g :<C-u>Denite grep:. -no-empty<CR>
nnoremap <leader>j :<C-u>DeniteCursorWord grep:.<CR>

" Define mappings while in 'filter' mode
"   <C-o>         - Switch to normal mode inside of search results
"   <Esc>         - Exit denite window in any mode
"   <CR>          - Open currently selected file in any mode
autocmd FileType denite-filter call s:denite_filter_my_settings()
function! s:denite_filter_my_settings() abort
  imap <silent><buffer> <C-o>
  \ <Plug>(denite_filter_quit)
  inoremap <silent><buffer><expr> <Esc>
  \ denite#do_map('quit')
  nnoremap <silent><buffer><expr> <Esc>
  \ denite#do_map('quit')
  inoremap <silent><buffer><expr> <CR>
  \ denite#do_map('do_action')
  inoremap <silent><buffer> <Down>
  \ <Esc><C-w>p:call cursor(line('.')+1,0)<CR><C-w>pA
  inoremap <silent><buffer> <Up>
  \ <Esc><C-w>p:call cursor(line('.')-1,0)<CR><C-w>pA
endfunction

" Define mappings while in denite window
"   <CR>        - Opens currently selected file
"   q or <Esc>  - Quit Denite window
"   d           - Delete currenly selected file
"   p           - Preview currently selected file
"   <C-o> or i  - Switch to insert mode inside of filter prompt
autocmd FileType denite call s:denite_my_settings()
function! s:denite_my_settings() abort
  nnoremap <silent><buffer><expr> <CR>
  \ denite#do_map('do_action')
  nnoremap <silent><buffer><expr> q
  \ denite#do_map('quit')
  nnoremap <silent><buffer><expr> <Esc>
  \ denite#do_map('quit')
  nnoremap <silent><buffer><expr> d
  \ denite#do_map('do_action', 'delete')
  nnoremap <silent><buffer><expr> p
  \ denite#do_map('do_action', 'preview')
  nnoremap <silent><buffer><expr> i
  \ denite#do_map('open_filter_buffer')
  nnoremap <silent><buffer><expr> <C-o>
  \ denite#do_map('open_filter_buffer')
endfunction


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => easy-motion
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"   <leader>w - Easy-motion highlights first word letters bi-directionally
map <leader><leader>w <Plug>(easymotion-bd-w)

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => coc.nvim
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

nmap <silent> <leader>cd <Plug>(coc-definition)
nmap <silent> <leader>cr <Plug>(coc-references)
nmap <silent> <leader>cj <Plug>(coc-implementation)
nnoremap <silent> <leader>ce  :<C-u>CocList diagnostics<cr>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => vim-better-whitespace
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"   <leader>y - Automatically remove trailing whitespace
nmap <leader>y :StripWhitespace<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => echodoc
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Enable echodoc on startup
let g:echodoc#enable_at_startup = 1

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => signify
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:signify_sign_delete = '-'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => colemak
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" nnoremap h h|xnoremap h h|onoremap h h|
" nnoremap n j|xnoremap n j|onoremap n j|
" nnoremap e k|xnoremap e k|onoremap e k|
" nnoremap i l|xnoremap i l|onoremap i l|
"
" nnoremap j i|
" nnoremap J I|
"
" nnoremap k n|
" nnoremap K N|

